log_level = "debug"
disable_mlock = true

ui = true

default_lease_ttl = "168h"
max_lease_ttl     = "720h"

api_addr = "https://vault.swigg.net"

listener "tcp" {
  address     = "0.0.0.0:8200"
  tls_disable = 1
}

storage "file" {
  path = "/vault/file"
}
